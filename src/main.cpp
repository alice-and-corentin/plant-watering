#include <Arduino.h>
#include <HTTPClient.h>
#include <WiFi.h>
#include <ArduinoJson.h>
#include "WaterLevel.h"
#include "Watering.h"
#include "HouseControllerClient.h"

const String SSID = "[insert your ssid here]";
const String PASSWORD = "[insert your password here]";
const String HOUSECONTROLLER_API_URL = "[insert the API URL here]";
const String PLANT_ID = "[insert the plant ID here]";

unsigned long wateringDelay = 5000;
unsigned long previousTimeWatering = 0;

bool isSoilEnoughWatered();
void waterSoil();

void setup()
{
  Serial.begin(115200);
  while(!Serial);

  // todo : init pins 

  initializeConnection(SSID, PASSWORD, HOUSECONTROLLER_API_URL, PLANT_ID);
}

void loop()
{
  manageWaterLevel();
  managerWatering();
  postPlantState(waterLevel, soilMoisture);
  delay(1000);
}