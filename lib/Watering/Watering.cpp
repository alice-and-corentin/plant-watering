#include <Arduino.h>
#include <HTTPClient.h>
#include <WiFi.h>
#include <ArduinoJson.h>

#define RELAY_PIN 13
#define MOISTURE_SENSOR_SIGNAL_PIN A0

int soilMoisture;

bool isSoilEnoughWatered()
{
    soilMoisture = analogRead(MOISTURE_SENSOR_SIGNAL_PIN);
    //   Serial.println("[WATERING] Soil Moisture " + soilMoisture);
    return soilMoisture < 300;
}

void waterSoil()
{
    digitalWrite(RELAY_PIN, HIGH);
    delay(1000);
    digitalWrite(RELAY_PIN, LOW);
}

void managerWatering()
{
    while (!isSoilEnoughWatered())
    {
        Serial.println("[WATERING] Soil needs to be watered");
        waterSoil();
    }
}

// https://dev.housecontroller.cboulch.fr/plants/64f1eb98731949bc8ef69469/state