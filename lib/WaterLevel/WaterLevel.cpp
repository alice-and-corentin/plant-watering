#include <Arduino.h>
#include <HTTPClient.h>
#include <WiFi.h>
#include <ArduinoJson.h>

#define WATER_LEVEL_SENSOR_POWER_PIN 6
#define WATER_LEVEL_SENSOR_SIGNAL_PIN A5
#define MAX_WATER_LEVEL 500
#define MIN_WATER_LEVEL 0
#define LED_PIN 3

int value = 0; // variable to store the sensor value
double waterLevel;

void manageWaterLevel()
{
    digitalWrite(WATER_LEVEL_SENSOR_POWER_PIN, HIGH);  // turns the sensor ON
    delay(10);                                         // wait 10 milliseconds
    value = analogRead(WATER_LEVEL_SENSOR_SIGNAL_PIN); // reads the analog value from sensor
    digitalWrite(WATER_LEVEL_SENSOR_POWER_PIN, LOW);   // turns the sensor OFF

    waterLevel = (double)(value - MIN_WATER_LEVEL) / (MAX_WATER_LEVEL - MIN_WATER_LEVEL);

    double brightness = (double)256.0 - (256.0 * waterLevel);
    if (brightness < 0.0)
    {
        brightness = 0;
    }
    else if (brightness > 255.0)
    {
        brightness = 255;
    }
    analogWrite(LED_PIN, brightness);

    // Serial.println("[WATER LEVEL] ");
    // Serial.print(waterLevel);
    // Serial.println();
}