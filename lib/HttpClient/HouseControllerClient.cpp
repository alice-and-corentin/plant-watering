#include <Arduino.h>
#include <HTTPClient.h>
#include <WiFi.h>
#include <ArduinoJson.h>

String baseUrl;
String plantIdentifier;

void post(String route, DynamicJsonDocument document)
{
    HTTPClient client;
    client.begin(route);

    String json;
    serializeJson(document, json);

    Serial.println(json);

    client.addHeader("Content-Type", "application/json");
    client.POST(json);

    Serial.println(client.getString());
}

void initializeConnection(String ssid, String password, String houseControllerUrl, String plantId)
{
    WiFi.begin(ssid, password);
    Serial.println("Connecting to WiFi");
    while (WiFi.status() != WL_CONNECTED)
    {
        delay(100);
        Serial.print(".");
    }

    Serial.print("Connected to WiFi network");

    baseUrl = houseControllerUrl;
    plantIdentifier = plantId;
}

void postPlantState(double waterLevel, int soilMoisture)
{
    DynamicJsonDocument doc(1024);
    doc["waterLevel"] = waterLevel;
    doc["moisture"] = soilMoisture;

    post(baseUrl + "/plants/" + plantIdentifier + "/state", doc);
}
